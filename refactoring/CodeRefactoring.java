package refactoring;

import java.util.Arrays;
import java.util.Scanner;

/*
 * Класс создан для рефакторинга и оптимизации линейного поиска
 *
 * @autor Tropanova N.S.
 */
public class CodeRefactoring {



    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите длину массива: ");
        int size = scanner.nextInt(); // Читаем с клавиатуры размер массива и записываем в size
        int[] array = new int[size]; // Создаём массив int размером в size
       // randomElements(array);

        outArray(array);

        System.out.println("Введите число для поиска: ");
        int number = scanner.nextInt();
        checksArrayElement(array, number);
        outputAnIndex(array,number);

    }

    /**
     * Метод, который проверяем наличие искомого эл-та в массиве
     *
     * @param array массив целых чисел
     */
    private static int checksArrayElement(int[] array, int number) {
        int temp = -1;
        for (int count = 0; count < 5; count++) {
            if (array[count] == number) {
                return count;
            }
        }
        return temp;
    }

    /**
     * Метод, который выводит индекс найденного эл-та
     *
     * @param array  массив целых чисел
     * @param number введенное число пользователем
     */
    private static void outputAnIndex(int[] array, int number) {
     int temp = checksArrayElement(array,number);
        if (temp == -1) {
            System.out.println("такого значения нет ");
        } else {
            System.out.println("номер элемента в последовательности " + temp);
        }
    }

    /*
     * выводит элементы массива
     *
     * @param array массив элементов
     */
    private static void outArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }
}