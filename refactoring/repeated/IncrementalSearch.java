package refactoring.repeated;

import java.util.Arrays;
import java.util.Scanner;

/*
 * Класс создан для рефакторинга и оптимизации линейного поиска
 *
 * @autor Tropanova N.S.
 */
public class IncrementalSearch {
    private static final int MIN_ARRAY_VALUE = -25;
    private static final int RANGE_IN_ARRAY = 51;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Введите длину массива: ");
        int size = inputArraySize();
        int[] array = new int[size];
        fillsArray(array);

        outArray(array);

        System.out.println("Введите число для поиска: ");
        int number = scanner.nextInt();

        int result = searchNumber(array, number);
        if (result == -1) {
            System.out.println("Искомого значения нет в списке ");
        } else {
            System.out.println("Индекс искомого элемента в последовательности: " + result);
        }

    }


    /**
     * Метод, принимает от пользователя значение размерности массива
     * проверяет корректность введеннях данных
     *
     * @return корректный размер массива
     */
    private static int inputArraySize() {
        int size;
        do {
            size = scanner.nextInt();
        } while (size < 1 || size > 100);
        return size;
    }

    /**
     * Метод, который заполняет массив рандомными элементами в диапазоне MIN до MAX RANGE
     *
     * @param array массив целых чисел
     */
    private static void fillsArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN_ARRAY_VALUE + (int) (Math.random() * RANGE_IN_ARRAY);
        }
    }

    /**
     * Метод поиска необходимого пользователю индекса числа в массиве
     *
     * @param array массив
     * @return индекс искомого элемента или -1 если такого элемента нет в массиве
     */
    private static int searchNumber(int[] array, int number) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return i;
            }
        }
        return -1;
    }

    /*
     * выводит элементы массива
     *
     * @param array массив элементов
     */
    private static void outArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }
}
